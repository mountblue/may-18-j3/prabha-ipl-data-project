var matchesData;
var deliveriesData;
let openMatchesFile = function (event) {
    let input = event.target;
    let reader = new FileReader();
    reader.onload = function () {
        let text = reader.result;
        reader.result.substring(0, 200);
        matchesData = toJson(text);
    };
    reader.readAsText(input.files[0]);


};


let openDeliveriesFile = function (event) {
    let input = event.target;

    let reader = new FileReader();
    reader.onload = function () {
        let text = reader.result;
        reader.result.substring(0, 200);
        deliveriesData = toJson(text);
    };
    reader.readAsText(input.files[0]);


};

function toJson(csvData) {
    let lines = csvData.split("\n");
    let colNames = lines[0].split(",");
    let records = [];
    alert('today is a very gud day :)');
    for (let i = 1; i < lines.length - 1; i++) {
        let record = {};
        let bits = lines[i].split(",");
        for (let j = 0; j < bits.length; j++) {
            record[colNames[j]] = bits[j];
        }
        records.push(record);
    }
    console.log(records);
    return records;
}

function numberOfMatches() {
    if (!matchesData) {
        alert("choose a csv file to plot a graph");
    } else {
        let chartName = "Number of Matches";
        let subTitle = "Over all Years";
        console.log(matchesData);
        let season = [];
        for (let i = 0; i < matchesData.length; i++) {
            season[i] = matchesData[i]['season'];
        }
        console.log(season);
        let uniqueSeason = getUniqueData(season);
        console.log(uniqueSeason);
        let c = 0;
        let numberOfMatches = [];
        for (let i = 0; i < uniqueSeason.length; i++) {
            for (let j = 0; j < matchesData.length; j++) {
                if (matchesData[j]['season'] === uniqueSeason[i])
                    c++;
            }
            numberOfMatches[i] = c;
            c = 0;
        }
        console.log(numberOfMatches);

        plotGraph(chartName, subTitle, uniqueSeason, numberOfMatches);

    }
}

function getUniqueData(inputArray) {
    var arr = [];
    for (var i = 0; i < inputArray.length; i++) {
        if (!arr.includes(inputArray[i])) {
            arr.push(inputArray[i]);
        }
    }
    arr.sort();
    return arr;
}

function matchesWonByEachTeam() {
    if (!matchesData) {
        alert("choose a csv file to plot a graph");
    } else {

        let chartName = "Number of Matches Won By Each Team";
        let subTitle = "Over all Years";
        let teamName = [];
        let season = [];

        var winner = [];
        for (let i = 0; i < matchesData.length; i++) {
            season[i] = matchesData[i]['season'];
        }

        let uniqueSeason = getUniqueData(season);

        let d = 1;
        for (let i = 0; i < matchesData.length; i++) {
            teamName[i] = matchesData[i]['team1'];
        }
        console.log(teamName);
        let uniqueTeamName = getUniqueData(teamName);
        console.log(uniqueTeamName);
        let count = 0;
        let values = {};
        let winnerPerYear = [];

        for (let i = 0; i < uniqueTeamName.length; i++) {
            for (let j = 0; j < uniqueSeason.length; j++) {
                for (let k = 0; k < matchesData.length; k++) {
                    if ((matchesData[k]['season'] === uniqueSeason[j]) && (matchesData[k]['winner'] ===
                            uniqueTeamName[i])) {
                        count++;
                    }
                }
                winner[j] = count;
                count = 0;
            }
            winnerPerYear[i] = winner;
            winner = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        }
        console.log(winnerPerYear);
        plotStackedBarChart(chartName, subTitle, uniqueSeason, uniqueTeamName, winnerPerYear);
    }
}

function extraRunsInTheYear() {
    if ((!matchesData) && (!deliveriesData)) {
        alert("choose a csv file to plot a graph");
    } else {
        let chartName = "Extra runs conceded per team";
        let subTitle = "In 2016";
        let matchId = [];
        let teamName = [];
        for (let i = 0; i < matchesData.length; i++) {
            teamName[i] = matchesData[i]['team1'];
        }
        console.log(teamName);
        let uniqueTeamName = getUniqueData(teamName);


        for (let i = 0; i < matchesData.length; i++) {
            if (matchesData[i]["season"] === "2016")
                matchId.push(matchesData[i]['id']);
        }

        let extraRuns = [];
        let c = 0;
        let name=[];
        for (let i = 0; i < uniqueTeamName.length; i++) {
            for (let j = 0; j < matchId.length; j++) {
                for (let k = 0; k < deliveriesData.length; k++) {
                    if ((deliveriesData[k]["match_id"] === matchId[j]) && (deliveriesData[k]["bowling_team"] ===
                            uniqueTeamName[i])) {
                        c = c + parseInt(deliveriesData[k]["extra_runs"]);
                    }
                }
            }
            if(c!==0){
                name.push(uniqueTeamName[i]);
                extraRuns.push(c);
            c = 0;
            }
            
        }
        console.log(extraRuns);
        console.log(name)
        plotGraph(chartName, subTitle, name, extraRuns);
    }


}

function topEconomicalBowlers() {
    if ((!matchesData) && (!deliveriesData)) {
        alert("choose a csv file to plot a graph");
    } else {
        let chartName = "Top Economical Bowlers";
        let subTitle = "In 2015";
        let matchId = [];
        let bowlersName = [];
        for (let i = 0; i < matchesData.length; i++) {
            if (matchesData[i]["season"] === "2015") {
                matchId.push(matchesData[i]['id']);
            }
        }

        for (let j = 0; j < matchId.length; j++) {
            for (let i = 0; i < deliveriesData.length; i++) {
                if (deliveriesData[i]["match_id"] === matchId[j]) {
                    bowlersName.push(deliveriesData[i]['bowler']);
                }
            }
        }

        let uniqueBowlersName = getUniqueData(bowlersName);
        let c = 0;
        let runs = 0;
        let balls = 0;
        let overs = 0;
        let total;
        let economicalRate = [];
        for (let i = 0; i < uniqueBowlersName.length; i++) {
            for (let j = 0; j < matchId.length; j++) {
                for (let k = 0; k < deliveriesData.length; k++) {
                    if ((deliveriesData[k]["match_id"] === matchId[j]) && (deliveriesData[k]["bowler"] ===
                            uniqueBowlersName[i])) {
                        runs = runs + parseInt(deliveriesData[k]["total_runs"]);
                        if ((deliveriesData[k]["wide_runs"] === '0') || (deliveriesData[k]["noball_runs"] ===
                                '0')) {
                            balls++;
                        }

                    }
                }
            }
            overs = balls / 6;
            total = runs / overs;
            economicalRate.push(total);
            runs = 0;
            balls = 0;
            overs = 0;
            total = 0;
        }

        let bowlerDetails = [];
        for (let i = 0; i < uniqueBowlersName.length; i++) {
            let econRate = {};
            econRate["name"] = uniqueBowlersName[i];
            econRate["econ"] = economicalRate[i];
            bowlerDetails.push(econRate);
        }
        bowlerDetails.sort(function (obj1, obj2) {
            return obj1.econ - obj2.econ;
        });


        let topTenBowlerName = [];
        let topTenBowlerEcon = [];
        for (let i = 0; i < 10; i++) {
            topTenBowlerName.push(bowlerDetails[i]["name"]);
            topTenBowlerEcon.push(bowlerDetails[i]["econ"])
        }
        console.log(topTenBowlerName);
        console.log(topTenBowlerEcon);
        plotGraph(chartName, subTitle, topTenBowlerName, topTenBowlerEcon);
    }

}

function plotGraph(chartName, subTitle, category, values) {

    var chart = Highcharts.chart('container', {

        title: {
            text: chartName
        },

        subtitle: {
            text: subTitle
        },

        xAxis: {
            categories: category
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: values,
            showInLegend: false
        }]

    });
}

function plotStackedBarChart(chartName, subTitle, uniqueSeason, uniqueTeamName, winnerPerYear) {
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: chartName
        },

        subtitle: {
            text: subTitle
        },

        xAxis: {
            categories: uniqueSeason
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Performance'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            reversed: true
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal'

            }
        },
        series: [{
                name: uniqueTeamName[0],
                data: winnerPerYear[0]

            }, {
                name: uniqueTeamName[1],
                data: winnerPerYear[1]

            },
            {
                name: uniqueTeamName[2],
                data: winnerPerYear[2]

            },
            {
                name: uniqueTeamName[3],
                data: winnerPerYear[3]

            },
            {
                name: uniqueTeamName[4],
                data: winnerPerYear[4]

            },
            {
                name: uniqueTeamName[5],
                data: winnerPerYear[5]

            },
            {
                name: uniqueTeamName[6],
                data: winnerPerYear[6]

            },
            {
                name: uniqueTeamName[7],
                data: winnerPerYear[7]

            },
            {
                name: uniqueTeamName[8],
                data: winnerPerYear[8]

            },
            {
                name: uniqueTeamName[9],
                data: winnerPerYear[9]

            },
            {
                name: uniqueTeamName[10],
                data: winnerPerYear[10]

            },
            {
                name: uniqueTeamName[11],
                data: winnerPerYear[11]

            },
            {
                name: uniqueTeamName[12],
                data: winnerPerYear[12]

            },
            {
                name: uniqueTeamName[13],
                data: winnerPerYear[13]

            }

        ]
    });
}